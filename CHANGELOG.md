## [1.0.1] - 2020/10/18
### Changed
- Changed some stuff
- Changed some more stuff
### Deprecated
- Deprecated some stuff

## [1.0.0] - 2020/10/02
### Added
- Added a new feature

## [0.9.4] - 2020/09/27
### Fixed
- Fixed a bug
